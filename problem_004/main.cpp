#include <iostream>
#include <string>

using namespace std;

int reverse_int(int n);
bool is_palindrome(int n);

int main(int argc, char *argv[])
{
    int max=0;
    for(int i=111;i<=999;++i)
        for(int j=111;j<=i;++j){
            int prod = i*j;
            if(is_palindrome(prod))
                max = (prod>max) ? prod : max;
        }
    cout << max << endl;
    return 0;
}

int reverse_int(int n){
    int reverse = 0;
    while(n != 0){
            int remainder = n%10;
            reverse = reverse*10 + remainder;
            n/=10;
    }
    return reverse;
}

bool is_palindrome(int n){
    return (n==reverse_int(n));
}
