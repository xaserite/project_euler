#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int N = 1000, sum = 0;
    for(int i=0;i<N;++i){
        if(i%3==0){
            sum+=i;
            continue;
        }
        if(i%5==0){
            sum+=i;
        }
    }
    cout << sum << endl;
    return 0;
}
