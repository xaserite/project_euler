#include <iostream>

using namespace std;

int sum_of_squares(int N);
int square_of_sum(int N);

int main(int argc, char *argv[])
{
    int N = 100;
    cout << square_of_sum(N) - sum_of_squares(N) << endl;
    return 0;
}
 int sum_of_squares(int N){
     int sum = 1;
     for(int i=2;i<=N;++i)
         sum += i*i;
     return sum;
 }

 int square_of_sum(int N){
     int sum = 1;
     for(int i=2;i<=N;++i)
         sum += i;
     return sum*sum;
 }
