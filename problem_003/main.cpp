#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char *argv[])
{
    unsigned long long N = 600851475143, n = sqrt(N), max=2;
    for(unsigned long long i=2;i<n;++i)
        if(N%i==0){
            max = N;
            N /=i;
        }
    cout << max << endl;
    return 0;
}
