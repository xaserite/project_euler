#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    int F1 = 1, F2 = 2, F3, F4, F5 = 0, sum = 2, N = 4000000;
    while(F5<=N){
        F3 = F1+F2;
        F1 = F4 = F2+F3;
        F2 = F5 = F3+F4;
        if(F5<=N) sum += F5;
    }
    cout << sum << endl;
    return 0;
}
// 0x00x00x00x00x00x00x00x00x00x
